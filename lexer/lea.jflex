package org;

import java.io.*;
import java_cup.runtime.*;


%%

%public
%class LeaLexer
%line
%column
%cupsym LeaSymbol
%cup
%8bit
%standalone

%{
	private Symbol symbol (int type) {
  		return new Symbol(type, yyline, yycolumn);
  	}

	private Symbol symbol (int type, Object value) {
  		return new Symbol(type, yyline, yycolumn,value);
  	}

  	static protected void trace(String str) {
		System.err.println("*** " + str);
	}
  	
  	StringBuffer str = new StringBuffer();
%}

LineTerminator = \r|\n|\r\n
InputCharacter = [^\n\r]
WhiteSpace = {LineTerminator}|[ \f\t]

IntegerLiteral = 0 | [1-9][0-9]*

FloatLiteral =  (({IntegerLiteral} "." [0-9]*) | ("." [0-9]+)) [eE][-+]?{IntegerLiteral}?

Character = [^\r\n]
CharacterLiteral = "'" {Character} "'"

CommentOneLine = "//" {Character}* {LineTerminator}
CommentMultipleLines = "/*" [^*] ~"*/" | "/*" "*"+ "/"
Comment = {CommentOneLine} | {CommentMultipleLines}

%state STRING_LITERAL COMMENT_DOC

%%

<YYINITIAL> {
	/*	S�parateur Op�rateur	*/
	
	"("		{LeaLexer.trace("GPAR"); return symbol(LeaSymbol.GPAR);}
	")" 	{LeaLexer.trace("DPAR"); return symbol(LeaSymbol.DPAR);}
	"{" 	{LeaLexer.trace("GACCO"); return symbol(LeaSymbol.GACCO);}
	"}" 	{LeaLexer.trace("DACCO"); return symbol(LeaSymbol.DACCO);}
	"[" 	{LeaLexer.trace("GCROCHET"); return symbol(LeaSymbol.GCROCHET);}
	"]" 	{LeaLexer.trace("DCROCHET"); return symbol(LeaSymbol.DCROCHET);}
	"&&" 	{LeaLexer.trace("ET"); return symbol(LeaSymbol.ET);}
	"||" 	{LeaLexer.trace("OU"); return symbol(LeaSymbol.OU);}
	"<" 	{LeaLexer.trace("PLUSPETIT"); return symbol(LeaSymbol.PLUSPETIT);}
	">" 	{LeaLexer.trace("PLUSGRAND"); return symbol(LeaSymbol.PLUSGRAND);}
	"<=" 	{LeaLexer.trace("INFOUEGAL"); return symbol(LeaSymbol.INFOUEGAL);}
	">=" 	{LeaLexer.trace("SUPOUEGAL"); return symbol(LeaSymbol.SUPOUEGAL);}
	"!=" 	{LeaLexer.trace("PASEGAL"); return symbol(LeaSymbol.PASEGAL);}
	"=" 	{LeaLexer.trace("EGAL"); return symbol(LeaSymbol.EGAL);}
	"+" 	{LeaLexer.trace("PLUS"); return symbol(LeaSymbol.PLUS);}
	"-" 	{LeaLexer.trace("MOINS"); return symbol(LeaSymbol.MOINS);}
	"*" 	{LeaLexer.trace("FOIS"); return symbol(LeaSymbol.FOIS);}
	"/" 	{LeaLexer.trace("DIVISE"); return symbol(LeaSymbol.DIVISE);}
	";" 	{LeaLexer.trace("POINTVIRGULE"); return symbol(LeaSymbol.POINTVIRGULE);}
	":" 	{LeaLexer.trace("DEUXPOINTS"); return symbol(LeaSymbol.DEUXPOINTS);}
	"," 	{LeaLexer.trace("VIRG"); return symbol(LeaSymbol.VIRG);}
	":=" 	{LeaLexer.trace("AFFECT"); return symbol(LeaSymbol.AFFECT);}
	"&" 	{LeaLexer.trace("ETCOM"); return symbol(LeaSymbol.ETCOM);}
	".." 	{LeaLexer.trace("POINTPOINT"); return symbol(LeaSymbol.POINTPOINT);}
	"." 	{LeaLexer.trace("POINT"); return symbol(LeaSymbol.POINT);}
	"~" 	{LeaLexer.trace("TILDE"); return symbol(LeaSymbol.TILDE);}
	"`" 	{LeaLexer.trace("GRAVE"); return symbol(LeaSymbol.GRAVE);}
	
	/*	Mots cl�s	*/
	
	"function" 	{LeaLexer.trace("FONCTION"); return symbol(LeaSymbol.FONCTION);}
	"procedure" {LeaLexer.trace("PROCEDURE"); return symbol(LeaSymbol.PROCEDURE);}
	"operator" 	{LeaLexer.trace("OPERATEUR"); return symbol(LeaSymbol.OPERATEUR);}
	
	"return" 	{LeaLexer.trace("RETURN"); return symbol(LeaSymbol.RETURN);}
	"if" 		{LeaLexer.trace("IF"); return symbol(LeaSymbol.IF);}
	"else" 		{LeaLexer.trace("ELSE"); return symbol(LeaSymbol.ELSE);}
	"while" 	{LeaLexer.trace("WHILE"); return symbol(LeaSymbol.WHILE);}
	"repeat" 	{LeaLexer.trace("REPEAT"); return symbol(LeaSymbol.REPEAT);}
	"for" 		{LeaLexer.trace("FOR"); return symbol(LeaSymbol.FOR);}
	"in" 		{LeaLexer.trace("IN"); return symbol(LeaSymbol.IN);}
	
	"char" 		{LeaLexer.trace("CARAC"); return symbol(LeaSymbol.CARAC);}
	"int" 		{LeaLexer.trace("ENTIER"); return symbol(LeaSymbol.ENTIER);}
	"float" 	{LeaLexer.trace("FLOAT"); return symbol(LeaSymbol.FLOAT);}
	"bool" 		{LeaLexer.trace("BOOLEAN"); return symbol(LeaSymbol.BOOLEAN);}
	"string" 	{LeaLexer.trace("STRING"); return symbol(LeaSymbol.STRING);}
	"enum" 		{LeaLexer.trace("ENUM"); return symbol(LeaSymbol.ENUM);}
	
	"struct" 	{LeaLexer.trace("STRUCT"); return symbol(LeaSymbol.STRUCT);}
	"true" 		{LeaLexer.trace("TRUE"); return symbol(LeaSymbol.TRUE);}
	"false" 	{LeaLexer.trace("FALSE"); return symbol(LeaSymbol.FALSE);}
	
	"null" 		{LeaLexer.trace("NULL"); return symbol(LeaSymbol.NULL);}
	
	"class" 	{LeaLexer.trace("CLASS"); return symbol(LeaSymbol.CLASS);}
	
	"private" 	{LeaLexer.trace("PRIVATE"); return symbol(LeaSymbol.PRIVATE);}
	"protected" {LeaLexer.trace("PROTECTED"); return symbol(LeaSymbol.PROTECTED);}
	"public" 	{LeaLexer.trace("PUBLIC"); return symbol(LeaSymbol.PUBLIC);}
	
	"static" 	{LeaLexer.trace("STATIC"); return symbol(LeaSymbol.STATIC);}
	
	/*	Strings	*/
	\" 		{yybegin(STRING_LITERAL); str.delete(0, str.length());}
	
	/*	Comments	*/
	"/**" 	{yybegin(COMMENT_DOC);	str.delete(0, str.length());}
	
	/* Keywords */
	return 	{System.out.println("KEYWORD: " +  yytext());}
	if		{System.out.println("KEYWORD: " +  yytext());}
	while	{System.out.println("KEYWORD: " +  yytext());}
	
	/* 	Operators 	*/
	"+"		{System.out.println("OPERATOR: " +  yytext());}
		
	/* Comments and whitespace */ 
	{WhiteSpace} 	{/*  ignore  */}
	.         		{ /* ignore */ }  
}

/*	Strings	*/
<STRING_LITERAL> {
	\" 			{
					yybegin(YYINITIAL);
					LeaLexer.trace("String LITERAL : " + str.toString());
					return symbol(LeaSymbol.STRING_LITERAL, str.toString());
				}
	\tt       	{str.append('\t');}
	\\n 		{str.append('\n');}
	\\r 		{str.append('\r');}
	\\\" 		{str.append('\"');}
	\\ 			{str.append('\\');}
} 

/*	Comments	*/
<COMMENT_DOC> {
	"**/"	{
				yybegin(YYINITIAL);
				LeaLexer.trace("COMMENT_DOC: " + str.toString());
				return symbol(LeaSymbol.COMMENT_DOC, str.toString());
			}
	(.|\n)  {str.append(yytext()); }
}