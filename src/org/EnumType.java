package org;

/**
 * Enumeration des types et de leur correspondance
 * @author tlegoahe
 *
 */

public enum EnumType {
	CHAR ("char"), 
	INT ("int"), 
	FLOAT ("float"),
	STRING ("string"), 
	BOOLEAN ("bool"), 
	ARRAY ("array"), 
	POINTER ("pointer"), 
	VOID ("void"),
	ERROR ("error");
	
    private final String tag;

    /**
     * Constructeur
     * @param s
     */
    EnumType(String s){
            tag = s;
    }

    /**
     * Conversion en string
     */
    public String toString(){
            return tag;
    }

}
