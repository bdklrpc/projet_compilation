package org;


import java.io.FileReader; 


import java_cup.runtime.Symbol;

/**
 * Classe Main, lancement du programme
 * @author mdouris
 *
 */
public class Main {
	
    static Env firstEnv=null;
    static Env currentEnv=null;
    
    /**
     * Constructeur du main
     * @param args
     */
    public static void main(String[] args) {
		try {
		    FileReader  myFile = new FileReader(args[0]);
		    LeaLexer myLex = new LeaLexer(myFile);
		    Parser myP = new Parser(myLex);
		    Symbol result=null;
		    try {
		    	result=myP.parse();
				try {
				    AbSynt root=(AbSynt)result.value;
				    //System.out.println(root.toString());
				    //root.toDot("detruire");
				    ToJava rootToConv = new ToJava(root);
				    rootToConv.startConvert();
				} catch (Exception e) {
				    System.out.println("result error");
				}
		    } catch (Exception e) {
		    	System.out.println("parse error...");
		    }
		} catch (Exception e){
		    System.out.println("invalid file");	    
		}
    }
}
