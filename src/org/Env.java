package org;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/* stack of scopes implementing the environnement */

/**
 * Classe permettant de separer les morceaux de code en environnements
 * @author tlegoahe
 *
 */
public class Env {
	protected static int nb=0;	
	protected static int scopenb=0;

	/* double linked list */
    private Env prev;
    private Env next;
    private ScopeTree root; /* scope on top of the stack */

    private int scopenum;

    /**
     * Constructeur
     * @param prev
     * @param next
     */
    public Env(Env prev, Env next) {
    	this.prev=prev;
    	this.next=next;
    	this.root=null;

    	this.scopenum=scopenb++;
    	//this(prev, next, null);
    }
    
    /**
     * Constructeur
     * @param prev
     */
    public Env(Env prev){
    	this(prev,null);
    }
    
    /**
     * Constructeur
     */
    public Env() {
    	this(null,null);
    }
    
    /**
     * Recuperation du numero de l'environnement
     * @return
     */
    public int getScopeNum() {
        return scopenum;
    }
    
    /**
     * Recuperation de l'environnement precedent
     * @return
     */
    public Env getPrev() {
	return prev;
    }
    
    /**
     * Affectation de l'environnement precedent
     * @param prev
     */
    public void putPrev(Env prev) {
	this.prev = prev;
    }
    
    /**
     * Recuperation de l'environnement suivant
     * @return
     */
    public Env getNext() {
	return next;
    }
    
    /**
     * Affectation de l'environnement suivant
     * @param next
     */
    public void putNext(Env next) {
	this.next = next;
    }
    
    /**
     * Recuperation de la racine de l'ABR
     * @return
     */
    public ScopeTree getRoot() {
	return root;
    }
    
    /**
     * Ajout d'element dans l'ABR
     * @param s
     * @param t
     */
    public void add(String s, Type t) {
	if (root==null){
	    if (prev!=null && prev.root!=null){
		root=new ScopeTree(prev.root.getLeft(), prev.root.getRight(), prev.root.getTag(), prev.root.getType()); 
		root=root.add(s, t, root);
	    } else {
		root=new ScopeTree(s, t);
		root=root.add(s, t, root);
	    }
	} else {		
	    root=root.add(s, t, root);
	}
    }

    /**
     * Recherche de donnee
     * @param s
     * @return
     */
    public Type find(String s){
	if (root!=null)
	    return root.find(s);
	else
	    return null;
    }

}
