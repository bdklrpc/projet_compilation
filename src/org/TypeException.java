package org;

/**
 * Classe pour gerer l'exception concernant le type
 * @author Alex
 *
 */
public class TypeException extends Exception{
	
	
	    private static final long serialVersionUID = 1L;
	    
	    private Type type1, type2;
	    private EnumTag tag;

	    /**
	     * Constructeur
	     * @param type1
	     * @param type2
	     * @param tag
	     */
	    public TypeException(Type type1, Type type2, EnumTag tag){
	    	this.type1 = type1;
	    	this.type2 = type2;
	    	this.tag = tag;
	    }

	    /**
	     * Recuperation du message d'erreur
	     */
        public String getMessage() {
            return "Type mismatch: "+ type1 +" "+ tag +" "+ type2;
        }
}
