package org;

/**
 * Classe pour gerer l'exception concernant l'environnement
 * @author tlegoahe
 *
 */
public class EnvException extends Exception{

    private static final long serialVersionUID = 1L;
    
    /**
     * Constructeur
     * @param mess
     */
    public EnvException(String mess){
    	System.out.println(mess);
    }

}
