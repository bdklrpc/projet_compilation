package org;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
// import org.tp.temp.*;

/**
 * Classe permettant de generer un Arbre Binaire de Recherche
 * @author tlegoahe
 *
 */
// binary search tree implementing a scope

public class ScopeTree {
    
    private ScopeTree left;
    private ScopeTree right;
    private int num;
    private String tag;	/* node label */
    private Type type;
    // private Temp tmp;  /* used in Intermediate Code */
    private boolean flag;
    
    /**
     * Constructeur
     * @param left
     * @param right
     * @param s
     * @param t
     */
    public ScopeTree(ScopeTree left, ScopeTree right, String s, Type t) {
	this.left=left;
	this.right=right;
	this.num=Env.nb++;
	this.tag=s;
	this.type=t;
	 // this.tmp=null;
	this.flag=false; // used in toDot
	//System.out.print(toString()+"\n"); 
    }
    
    /**
     * Constructeur
     * @param s
     * @param t
     */
    public ScopeTree(String s, Type t) {
	this(null, null, s ,t);
	//System.out.print(toString()+"\n"); 
    }
    
    /**
     * Recuperation de la partie gauche de l'ABR
     * @return
     */
    public ScopeTree getLeft() {
	return left;
    }
    
    /**
     * Affectation de la partie gauche de l'ABR
     * @param left
     */
    public void setLeft(ScopeTree left) {
	this.left = left;
    }
    
    /**
     * Recuperation de la partie gauche de l'ABR
     * @return
     */
    public ScopeTree getRight() {
	return right;
    }
    
    /**
     * Recuperation du numero d'ABR
     * @return
     */
    public int getNum() {
	return num;
    }
    
    /**
     * Recuperation du type
     * @return
     */
    public Type getType() {
	return type;
    }
    
    /**
     * Recuperation du tag
     * @return
     */
    public String getTag() {
	return tag;
    }
    
    /**
     * Affectation de la partie droite de l'ABR
     * @param right
     */
    public void setRight(ScopeTree right) {
	this.right = right;
    }
    
    /**
     * Recherche dans l'ABR
     * @param s
     * @return
     */
    public Type find(String s) {
	if (s.compareTo(tag) < 0)
	    return left.find(s);
	else if (s.compareTo(tag) > 0)
	    return right.find(s);
	else if (s.compareTo(tag) == 0)
	    return type; 
	else
	    return null;
    }
    
    /**
     * Ajout de donnee dans l'ABR
     * @param s
     * @param t
     * @param a
     * @return
     */
    public ScopeTree add(String s, Type t, ScopeTree a) {
	if (a==null)
	    return new ScopeTree(s, t);
	else if (s.compareTo(a.tag) < 0)
	    return new ScopeTree(add(s, t, a.left), a.right, a.tag, a.type);
	else if (s.compareTo(a.tag) > 0)
	    return new ScopeTree(a.left, add(s, t, a.right), a.tag, a.type);
	else
	    return new ScopeTree(a.left, a.right, a.tag, t);
    }

    /**
     * Conversion en string
     */
    public String toString() {
	String result = new String();
	result += tag;
	if ((left != null) || (right != null)){
	    result +="(";
	    if (left != null)
		result += left.toString();
	    result+=",";
	    if (right != null)
		result += right.toString();
	    result+=")";
	}
	return result;
    }
    
    
}
