package org;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Classe permettant la generation de l'arbre syntaxique
 * @author tlegoahe
 */

public class AbSynt {

   
    private AbSynt left;
    private AbSynt right;
    private EnumTag tag;	// node label
    private String str;     // used for identifiers
    private Env env;	    // current environnement
    private Type type;
    
    /**
     * Constructeur AbSynt
     * @param left
     * @param right
     * @param tag
     * @param t
     */
    
    public AbSynt(AbSynt left, AbSynt right, EnumTag tag, Type t) {
    	this.left=left;
    	this.right=right;
    	this.tag=tag;
    	this.str="";
    	this.env=Main.currentEnv;
    	this.type=t;
    	//System.out.print(toString()+"\n"); 
    }
    
    /**
     * Constructeur AbSynt 
     * @param left
     * @param right
     * @param tag
     */
    public AbSynt(AbSynt left, AbSynt right, EnumTag tag) 
	  /*throws TypeException*/ {
    	this(left, right, tag, null);
      	if(left.type != null && right.type != null)
      		if(!left.type.compareTo(right.type))
      			//throw new TypeException(left.type,right.type, tag);	
      			System.err.println("\n Type mismatch "+left.type+" and "+right.type
      					              +" in "+tag);
      		else
      			type = left.type;
        }
        
    /**
     * Constructeur AbSynt
     * @param tag
     */
    public AbSynt(EnumTag tag) {
    	this(null, null, tag, null);
    }
    
    /**
     * Constructeur AbSynt
     * @param tag
     * @param str
     * @param type
     */
    public AbSynt(EnumTag tag, String str, Type type) {
    	this(null, null, tag, type);	
    	this.str=str;
    }
    
    /**
     * Constructeur AbSynt
     * @param tag
     * @param str
     */
    public AbSynt(EnumTag tag, String str) {
    	this(null, null, tag, null);	
    	this.str=str;
    }
    
    /**
     * Recuperation de l'arbre ayant pour racine le fils gauche dans l'arbre
     * @return left
     */
    public AbSynt getLeft() {
	return left;
    }
    
    /**
     * Affectation de l'arbre qui aura pour racine le fils gauche dans l'arbre
     * @param left
     */
    public void setLeft(AbSynt left) {
	this.left = left;
    }
    
    /**
     * Recuperation de l'arbre ayant pour racine le fils droit dans l'arbre
     * @return right
     */
    public AbSynt getRight() {
	return right;
    }
    
    /**
     * Affectation de l'arbre qui aura pour racine le fils droit dans l'arbre
     * @param right
     */
    public void setRight(AbSynt right) {
	this.right = right;
    }
    
    /**
     * Recuperation du type
     * @return type
     */
    public Type getType() {
	return type;
    }
    
    /**
     * Affectation du type
     * @param type
     */
    public void putType(Type type) {
    this.type=type;
    }
    
    /**
     * Recuperation du tag
     * @return tag
     */
    public EnumTag getTag() {
        return tag;
    }    
    
    /**
     * Recuperation du nom
     * @return str
     */
    public String getName() {
        return str;
    }
    
    /**
     * Recuperation de l'environnement
     * @return env
     * @throws EnvException
     */
    public Env getEnv() throws EnvException {
        if (env == null)
        	throw new EnvException("current environnement is null");
        else
        	return env;
    }

    /**
     * Conversion en string
     */
    public String toString() {
		String result = new String();
		result += tag.toString();
		if ((left != null) || (right != null)){
		    result +="(";
		    if (left != null)
		    	result += left.toString();
		    if (right != null){
	                result+=",";
	                if (right.tag==EnumTag.SUCC || right.tag==EnumTag.ELSE)
	                    result+="\n\n\t";
		    	result += right.toString();
	            }
		    result+=")";
		}
		return result;
    }
    
}
