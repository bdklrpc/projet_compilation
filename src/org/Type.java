package org;




/**
 * Classe generant un arbre qui implemente un type
 * @author Alex
 *
 */
public class Type {
    
    private Type left;
    private Type right;
    private EnumType type;	
    private Integer integer; // array size
    
    /**
     * 
     * @param left
     * @param right
     * @param i
     * @param type
     */
    public Type(Type left, Type right, Integer i, EnumType type) {
	this.left=left;
	this.right=right;
	this.integer=i;
	this.type=type;
    }
    
    /**
     * 
     * @param left
     * @param right
     * @param type
     */
    public Type(Type left, Type right, EnumType type) {
    	this(left,right,0, type);
        }
        
    /**
     * 
     * @param type
     */
    public Type(EnumType type) {
    	this(null,null,type);
    }
    
    /**
     * 
     * @return
     */
    public Type getLeft() {
	return left;
    }
    
    /**
     * 
     * @param left
     */
    public void setLeft(Type left) {
	this.left = left;
    }
    
    /**
     * 
     * @return
     */
    public Type getRight() {
	return right;
    }
    
    /**
     * 
     * @param right
     */
    public void setRight(Type right) {
	this.right = right;
    }
    
    /**
     * 
     * @return
     */
    public EnumType getEnumType() {
    	return this.type;
    }
    
    /**
     * 
     * @param t
     * @return
     */
    public boolean compareTo(Type t){
    	switch (this.type) {
    	case CHAR:
    	case INT:
    	case FLOAT:
    	case STRING:
    	case BOOLEAN:
    		if (this.type == t.type)
                return true;
    		else return false;
    	case ARRAY:
    	case POINTER:
    		if (this.type == t.type)
                return this.left.compareTo(t.left);
    		else return false;
    	};
    	return false;	
    }

    /**
     * 
     */
    public String toString() {
	String result = new String();
	switch (type) {
	case CHAR:
	case INT:
	case FLOAT:
	case STRING:
	case BOOLEAN:
		result += type.toString();break;
	case ARRAY:result += "ARRAY ["+integer+"] OF "+left.toString(); break;
	case POINTER:result += "POINTER OF "+left.toString(); break;
	    //case ERROR:result += "ERROR"; break;
	};
	return result;
    }
}
