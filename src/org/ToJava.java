package org;

import java.io.BufferedWriter; 
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Classe permettant la generation du code Java
 * @author mdouris
 */
public class ToJava {
	
	private PrintWriter fileWriter;
	private AbSynt root;
	
	/**
	 * Constructeur ToJava
	 * @throws IOException
	 * @param root Type AbSynt, passage de la racine de l'arbre
	 */
	public ToJava(AbSynt root) throws IOException {
		fileWriter =  new PrintWriter(new BufferedWriter
				   (new FileWriter("Output.java")));
		this.root = root;
	}
	
	/**
	 * Creation du header du fichier
	 */
	public void fileHeaders() {
		fileWriter.println("import java.io.*;");
		fileWriter.println("public class Main {");
		fileWriter.println("public static void main(String[] args)");
	}
	
	/**
	 * Creation du footer du fichier
	 */
	public void fileFooters() {
		fileWriter.println("}");
		fileWriter.close();
	}
	
	/**
	 * Permet de lancer la conversion de l'arbre en Java depuis le main
	 */
	public void startConvert() {
		fileHeaders();
		convert(this.root);
		fileFooters();
	}
	
	/**
	 * Permet la conversion d'un noeud de l'arbre en langage Java
	 * @param node Noeud a traiter
	 */
	public void convert(AbSynt node) {
		switch(node.getTag()) {
		case IF:
			fileWriter.print("if(");
			this.convert(node.getLeft());
			fileWriter.println(")");
			this.convert(node.getRight());
			break;
		case ELSE:
			this.convert(node.getLeft());
			fileWriter.println("else");
			this.convert(node.getRight());
			break;
		case WHILE:
			fileWriter.print("while(");
			this.convert(node.getLeft());
			fileWriter.println(")");
			this.convert(node.getRight());
			break;
		case FOR:
			fileWriter.print("for(");
			this.convert(node.getLeft());
			fileWriter.println(")");
			this.convert(node.getRight());
			break;
		case AFFECT:
			this.convert(node.getLeft());
			fileWriter.print("=");
			this.convert(node.getRight());
			fileWriter.println(";");
			break;
		case ET:
			this.convert(node.getLeft());
			fileWriter.print(" && ");
			this.convert(node.getRight());
			break;
		case OU:
			this.convert(node.getLeft());
			fileWriter.print(" || ");
			this.convert(node.getRight());
			break;
		case PLUSPETIT:
			this.convert(node.getLeft());
			fileWriter.print(" < ");
			this.convert(node.getRight());
			break;
		case PLUSGRAND:
			this.convert(node.getLeft());
			fileWriter.print(" > ");
			this.convert(node.getRight());
			break;
		case INFOUEGAL:
			this.convert(node.getLeft());
			fileWriter.print(" <= ");
			this.convert(node.getRight());
			break;
		case SUPOUEGAL:
			this.convert(node.getLeft());
			fileWriter.print(" <= ");
			this.convert(node.getRight());
			break;
		case EGAL:
			this.convert(node.getLeft());
			fileWriter.print(" == ");
			this.convert(node.getRight());
			break;
		case PASEGAL:
			this.convert(node.getLeft());
			fileWriter.print(" != ");
			this.convert(node.getRight());
			break;
		case PLUS:
			this.convert(node.getLeft());
			fileWriter.print(" + ");
			this.convert(node.getRight());
			break;
		case MOINS:
			this.convert(node.getLeft());
			fileWriter.print(" - ");
			this.convert(node.getRight());
			break;
		case FOIS:
			this.convert(node.getLeft());
			fileWriter.print(" * ");
			this.convert(node.getRight());
			break;
		case DIVISE:
			this.convert(node.getLeft());
			fileWriter.print(" / ");
			this.convert(node.getRight());
			break;
		case CLASS:
			fileWriter.print("class ");
			this.convert(node.getLeft());
			fileWriter.print(" { ");
			this.convert(node.getRight());
			fileWriter.print(" } ");
			break;
		case FONCTION:
			fileWriter.print("function (");
			this.convert(node.getLeft());
			fileWriter.print(" ) { ");
			this.convert(node.getRight());
			fileWriter.print(" } ");
			break;
		case LIST:
			fileWriter.print("ArrayList<");
			this.convert(node.getLeft());
			fileWriter.print("> = new ArrayList<");
			this.convert(node.getLeft());
			fileWriter.print(">();");
			break;
		case OF:
			this.convert(node.getLeft());
			break;
		case VAR:
			fileWriter.print(node.getName());
			break;
		case BOOLEAN:
			fileWriter.print(node.getName());
			break;
		case ENTIER:
			fileWriter.print(node.getName());
			break;
		case CARAC:
			fileWriter.print(node.getName());
			break;
		case FLOAT:
			fileWriter.print(node.getName());
			break;
		case STRING:
			fileWriter.print("\""+ node.getName()+ "\"");
			break;
		case PRIVATE:
			fileWriter.print("private ");
			break;
		case PUBLIC:
			fileWriter.print("public ");
			break;
		case PROTECTED:
			fileWriter.print("protected ");
			break;
		case STATIC:
			fileWriter.print("static ");
			break;
		case TRUE:
			fileWriter.print("true ");
			break;
		case FALSE:
			fileWriter.print("false ");
			break;
		case NULL:
			fileWriter.print("null ");
			break;
		case SUCC:			
			fileWriter.println("{");
			this.convert(node.getLeft());
			this.convert(node.getRight());
			fileWriter.println("}");
			break;	
		}
	}
}
