package org;

/**
 * Enumeration des differents tags et de leur correspondance
 * @author tlegoahe
 *
 */
public enum EnumTag { 
	
    AFFECT (":="), 
    GPAR ("("),
    DPAR (")"),
    GACCO ("{" ),
    DACCO ("}"),
    RCROCHET ("["), 
    DCROCHET ("]"),
    ET ("&&"),
    OU ("||"), 
    PLUSPETIT ("<"),
    PLUSGRAND (">"), 
    INFOUEGAL ("<="), 
    SUPOUEGAL (">="), 
    PASEGAL ("!="),
    EGAL ("="),
    PLUS ("+"), 
    MOINS ("-"), 
    FOIS ("*"),
    DIVISE ("/"), 
    POINTVIRGULE (";" ),
    DEUXPOINTS (":"),
    VIRG (","),
    ETCOM ("&"), 
    POINTPOINT (".."), 
    POINT ("." ),
    TILDE ("~"),

    /*      Mots cles */
    
    IF ("if"), 
    WHILE ("while"),
    FONCTION ("function"),
    PROCEDURE ("procedure"), 
    OPERATOR ("operator" ),
    RETURN ("return" ),
    ELSE ("else"),
    REPEAT ("repeat"), 
    FOR ("for"),
    IN ("in"),
    CARAC ("char"), 
    ENTIER ("int" ),
    FLOAT ("float" ),
    BOOLEAN ("bool" ),
    STRING ("string" ),
    ENUM ("enum" ),
    STRUCT ("struct"), 
    LIST ("list"),
    SET ("set" ),
    MAP ("map"),
    OF ("of" ),
    TRUE ("true"), 
    FALSE ("false"), 
    NULL ("null" ),
    CLASS ("class" ),
    PRIVATE ("private"), 
    PROTECTED ("protected"), 
    PUBLIC ("public" ),
    STATIC ("static" ),
    VAR ("variable"),
	SUCC("succ");
	
	
    private final String tag;

    /**
     * Constructeur
     * @param s
     */
    EnumTag(String s){
            tag = s;
    }

    /**
     * Conversion en string
     */
    public String toString(){
            return tag;
    }


}

